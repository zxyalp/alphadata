# -*- coding: utf-8 -*-
"""
Created on 2018/5/24

@author: xing yan
"""
from flask import Blueprint

main = Blueprint('main', __name__)

from . import views, errors
