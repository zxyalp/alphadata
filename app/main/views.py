# -*- coding: utf-8 -*-
"""
Created on 2018/5/24

@author: xing yan
"""
from datetime import datetime
from flask import render_template, redirect, session, url_for, flash

from . import main
from .forms import UserForm
from .. import db
from ..models import User


@main.route('/', methods=('GET', 'POST'))
def index():
    return render_template('index.html', current_time=datetime.utcnow())

